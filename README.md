# dice12

Idee: 
Für jede Runde pnp wird ein eigener dice12 container generiert. Nutzer können den Container selbst clonen und starten.

Alles wird lokal in der dice12 box gespeichert, Runden können pausiert und anderweitig fortgesetzt werden.

## Ansichten

1. Log-In
   1. Gamemasterlogintoken beim Starten vom Container generieren. Wer den Container hostet ist der Gamemaster
2. Anmeldung
   1. Login per Einladungstoken vom Gamemaster. Der wird generiert, der Gamemaster legt fest, wie der Nutzer heißt. 
   2. Passwort vergeben - Nicht notwendig. Der Gamemaster läd vor jedem Spiel per Login-Token ein.
3. Charakter
   1. Charakter erstellen:
      1. Virtueller Charakterbogen
   2. Charakter ingame
      1. Virtueller Charakterbogen, Änderungen durch durchgestrichene Werte etc. kennzeichnen. Gamemaster muss Änderungen durch Nutzer bestätigen, bevor was passiert.
4. Spielansicht
   1. Spielfeld in der Mitte mit großem Raster. Für das Spielfeld können Hintergrundbilder festgelegt werden
   2. Spieler sitzen am Bildschirmrand, Benutzerprofil wird durch Charakterbild aus Bogen symbolisiert. 
      Unterm Benutzerbild ist eine Spielfigur, die auf das Spielfeld gezogen werden kann.
   3. Sprachchat (mumble)-Integration. Wenn der Nutzer spricht, wird ein Sprachsymbol neben seinem Bild angezeigt
5. Gamemaster
   1. Selbe Ansicht wie Spielansicht
   2. Kann Hintergründe für das Raster festlegen
   3. Geometrische Formen für Monster/etc. in Taskbar, können wie Spielfiguren auf dem Feld platziert werden
6. Spielübersicht
   1. Spieldauer & Runden
   2. History

## Umsetzung

Die Sache soll schnell umgesetzt werden. 
Deshalb wird als Serveranwendung laravel mit blade benutzt. Frontend komplett in blade mit vue.
Backend komplett laravel, so viele fertige Bausteine von laravel wie möglich.

Der Container wird als docker-container gebaut, sodass der gamemaster mit docker run ein Spiel starten kann.

### Docker

Eigenes Laufwerk für Hochgeladene Dateien

Eigenes Laufwerk für Datenbank



